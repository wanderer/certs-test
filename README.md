# [certs-test](https://git.dotya.ml/wanderer/certs-test/)

this repo contains a program that tries to connect to a TLS protected endpoint.
there are two variants of a `Containerfile`:
* `Containerfile` - here the program should be able to successfully connect
to a TLS-protected endpoint from a `scratch` container because the **ca-certs**
bundle has been copied in.
* `Containerfile.certfail` - here the **ca-certs** bundle is missing,
therefore the program should fail with the following:
  > 2023/03/08 18:31:00 connecting to https://\<endpoint\>
  > panic: Get "https://\<endpoint\>": tls: failed to verify certificate: x509: certificate signed by unknown authority

### build and run using Go
to manually build and run the app, run:
```sh
go build -v . && ./certs-test -endpoint=<yoursite.tld>
```

### build and run using podman
to build a container in which the app will be able to connect to a TLS
connected endpoint, run:

```sh
podman build -tcerts-test:success -f Dockerfile .
```
alternatively, run `make` or `make certsuccess`

to build a container that will yield a CA cert validation failure (because of
the missing **ca-cert** bundle), run:

```sh
podman build -tcerts-test:fail -f Dockerfile .
```
alternatively, run `make certfail`

to run the container, do
```
podman run localhost/certs-test:success
```
or

```
podman run localhost/certs-test:fail
```
based on which one you'd like to run.

### build and run using Nix
build the flake's `success` and `failure` containers:
```sh
# build "success" first.
nix build .#success
podman load <result
# run the container loaded in the previous step.
podman run --rm localhost/certs-test:nix-success

# build the "failure".
nix build .#failure
# load the newly built container and run it.
podman load <result && \
podman run --rm localhost/certs-test:nix-fail

# inspect the containers.
podman inspect localhost/certs-test:nix-{success,fail}
```

build and run the app directly:
```sh
nix build .#certs-test && ./result/bin/certs-test
```

### LICENSE
CC0
