{
  description = "test that the ca-certs bundle works in scratch containers";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    nix-filter = {
      url = "github:numtide/nix-filter";
    };
  };

  outputs = {
    self,
    nixpkgs,
    nix-filter,
    ...
  }: let
    projname = "certs-test";

    # to work with older version of flakes
    lastModifiedDate =
      self.lastModifiedDate or self.lastModified or "19700101";

    # Generate a user-friendly version number.
    version = "v0.0.0";

    supportedSystems = ["x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin"];
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
    pkgs = forAllSystems (system: nixpkgs.legacyPackages.${system});
    # Nixpkgs instantiated for supported system types.
    nixpkgsFor = forAllSystems (system:
      import nixpkgs {
        inherit system;
        overlays = [
          # no overlay imports atm
          # (import ./overlay.nix)
        ];
      });
  in {
    formatter = forAllSystems (
      system:
        nixpkgsFor.${system}.alejandra
    );
    packages = forAllSystems (system: let
      baseurl = "https://git.dotya.ml/wanderer/certs-test/";
      pkgs = nixpkgsFor.${system};
      inherit (pkgs) lib overlays;
    in rec {
      certs-test = with pkgs;
        buildGo120Module rec {
          pname = "certs-test";
          buildInputs = [
            go_1_20
            gcc
          ];
          nativeBuildInputs = [pkgconfig];

          overrideModAttrs = _: {
            # GOPROXY = "direct";
            GOFLAGS = "-buildmode=pie -trimpath -mod=readonly -modcacherw";
            CGO_ENABLED = "0";
          };

          inherit version;
          doCheck = false;
          # use go.mod for managing go deps, instead of vendor-only dir
          proxyVendor = true;
          tags = []; # go "-tags" to build with
          ldflags = [
            "-s"
            "-w"
            "-X main.version=${version}"
          ];

          # dont't forget to update vendorSha256 whenever go.mod or go.sum change
          # vendorSha256 = "sha256-Ns3ohAzZliK75fM6ryWubhfLBCVwU7CsZbuuzZrGaRY=";
          vendorSha256 = null;

          # In 'nix develop', we don't need a copy of the source tree
          # in the Nix store.
          src = nix-filter.lib.filter {
            # when in doubt, check out
            # https://github.com/numtide/nix-filter#design-notes
            # tl;dr: it'd be best to include folders, however there are
            # currently issues with that approach.
            root = lib.cleanSource ./.;
            exclude = [
              ./README.md

              ./certs-test

              ./flake.nix
              ./flake.lock
              ./default.nix
              ./shell.nix

              ./README.md

              ./.envrc
              ./.gitattributes
              ./.gitignore

              # nix result symlink
              ./result

              # the entire .git folder
              ./.git
            ];
          };

          meta = {
            description = "certs-test";
            homepage = baseurl;
            license = lib.licenses.gpl3;
            maintainers = ["wanderer"];
            platforms = lib.platforms.linux ++ lib.platforms.darwin;
          };
        };

      scratch-with-cacerts = with pkgs;
        pkgs.dockerTools.pullImage {
          imageName = "ghcr.io/mariouhrik/scratch-with-cacerts";
          imageDigest = "sha256:4c95be74f178c9230587a557a1429f4bd10c4fdf24ddcc70a090d42f462ece55";
          sha256 = "sha256-1yLq1KcAl6xxFhRCPcWrCXzDs0Ik6+VAcfi/1MVDq38=";
          os = "linux";
          arch = "${system}";
        };

      success = with pkgs;
        pkgs.dockerTools.buildLayeredImage {
          name = "certs-test";
          tag = "nix-success";
          fromImage = scratch-with-cacerts;
          contents = [
            certs-test
          ];
          config = {
            Cmd = ["/bin/certs-test"];
          };
        };

      failure = with pkgs;
        dockerTools.buildLayeredImage {
          name = "certs-test";
          tag = "nix-fail";
          contents = [
            certs-test
          ];
          config = {
            Cmd = ["/bin/certs-test"];
          };
        };

      default = certs-test;
    });

    apps = forAllSystems (system: rec {
      certs-test = {
        type = "app";
        program = "${self.packages.${system}.${projname}}/bin/certs-test";
      };
      success = {
        type = "app";
        program = "${self.packages.${system}.${projname}}/bin/success";
      };
      failure = {
        type = "app";
        program = "${self.packages.${system}.${projname}}/bin/failure";
      };
      default = certs-test;
    });

    devShells = forAllSystems (
      system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            # (import ./overlay.nix)
          ];
        };
        upcache = pkgs.writeShellScriptBin "upcache" ''
          ## refs:
          ##   https://fzakaria.com/2020/08/11/caching-your-nix-shell.html
          ##   https://nixos.wiki/wiki/Caching_nix_shell_build_inputs
          nix-store --query --references $(nix-instantiate shell.nix) | \
            xargs nix-store --realise | \
            xargs nix-store --query --requisites | \
            cachix push ${projname}
          nix build --json \
            | jq -r '.[].outputs | to_entries[].value' \
            | cachix push ${projname}
        '';
        add-license = pkgs.writeShellScriptBin "add-license" ''
          go run github.com/google/addlicense@v1.0.0 -v \
            -c "wanderer <a_mirre at utb dot cz>" \
            -l "CC0" -s .
        '';
      in {
        default = with pkgs;
          mkShellNoCC {
            name = "${projname}-" + version;

            GOFLAGS = "-buildmode=pie -trimpath -mod=readonly -modcacherw";
            GOLDFLAGS = "-s -w -X main.version=${version}";
            # CGO_CFLAGS = "-g0 -Ofast -mtune=native -flto";
            # CGO_LDFLAGS = "-Wl,-O1,-sort-common,-as-needed,-z,relro,-z,now,-flto -pthread";
            CGO_ENABLED = "0";

            shellHook = ''
              echo " -- in ${projname} dev shell..."
            '';

            packages = [
              pre-commit
              statix

              # built-in
              upcache
              addlicense

              # deps
              go_1_20
              go-tools
              gopls
              gofumpt
            ];
          };
      }
    );
  };
}
