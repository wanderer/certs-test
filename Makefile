cmd = podman

.PHONY: default certsuccess certfail

default: certsuccess

certsuccess:
	$(cmd) build --no-cache -t certs-test:success -f Containerfile .

certfail:
	$(cmd) build --no-cache -t certs-test:fail -f Containerfile.certfail .
