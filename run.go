package main

import (
	"flag"
	"log"
	"net/http"
	"strings"
)

var endpoint = flag.String("endpoint", "", "endpoint to connect to over TLS, e.g. claudie.io (automatically upgraded to https://claudie.io)")

func run() error {
	prefix := "https://"
	s := prefix + "claudie.io"

	flag.Parse()

	if *endpoint != "" {
		log.Printf("flag: %s", *endpoint)
		s = *endpoint
	}

	// upgrade to https by default.
	s = strings.Replace(s, "http://", prefix, 1)

	// if there is no 'https://', add the prefix.
	if !strings.HasPrefix(s, prefix) {
		s = prefix + s
	}

	log.Printf("connecting to %s", s)

	resp, err := http.Head(s)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	log.Println("response status:", resp.Status)

	return nil
}
